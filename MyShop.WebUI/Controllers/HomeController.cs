﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MyShop.WebUI.Controllers
{
    public class HomeController : Controller
    {
        //
        // GET: /Home/

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Contact()
        {
            return View();
        }
        public ActionResult Payment()
        {
            return View();
        }


        public ActionResult Shipping()
        {
            return View();
        }

        public ActionResult Discounts()
        {
            return View();
        }
    }
}
